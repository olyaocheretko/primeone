//Adaptive functions  (1 appendTo 2)
$(window).resize(function(event){
	adaptive_function();
	}
);
function adaptive_header(w,h) {
		var headerMenu=$('.header-menu');
		var headerLang=$('.header-top-lang');
	if(w<767.98){
		if (!headerLang.hasClass('done')) {
			headerLang.addClass('done').appendTo(headerMenu);
		}
	}
	else{
		if (headerLang.hasClass('done')) {
			headerLang.removeClass('done').prependTo($('.header-top'));
		}
	}

	if(w<767.98){
		if (!$('.header-bottom-menu-list').hasClass('done')) {
			$('.header-bottom-menu-list').addClass('done').appendTo(headerMenu);
		}
	}
	else{
		if ($('.header-bottom-menu-list').hasClass('done')) {
			$('.header-bottom-menu-list').removeClass('done').appendTo($('.header-bottom-column-1'));
			$('.header-bottom-menu-list--right').appendTo($('.header-bottom-column-3'));
		}
	}
}
function adaptive_function() {
		var w=$(window).outerWidth();
		var h=$(window).outerHeight();
	adaptive_header(w,h);
}
	adaptive_function();